library;

import 'package:elevenlabs_sdk/src/api/text_speech_api.dart';
import 'package:elevenlabs_sdk/src/api/voices_api.dart';
import 'package:http/http.dart' as http;

export 'src/model/voice.dart';

const _baseUrl = 'https://api.elevenlabs.io';

class ElevenLabsSDK {
  ElevenLabsSDK({
    required String apiKey,
    http.Client? client,
    bool logging = false,
  })  : _apiKey = apiKey,
        _logging = logging {
    _inner = client ?? http.Client();
  }

  final String _apiKey;

  final bool _logging;

  late final http.Client _inner;

  late final VoicesAPI voices = VoicesAPI(
    baseUrl: _baseUrl,
    apiKey: _apiKey,
    client: _inner,
    logging: _logging,
  );

  late final TextSpeechAPI textSpeech = TextSpeechAPI(
    baseUrl: _baseUrl,
    apiKey: _apiKey,
    client: _inner,
    logging: _logging,
  );
}

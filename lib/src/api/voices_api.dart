import 'dart:convert';
import 'dart:io';

import 'package:elevenlabs_sdk/src/model/voice.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:logging/logging.dart';

class VoicesAPI {
  VoicesAPI({
    required String baseUrl,
    required String apiKey,
    required http.Client client,
    bool logging = false,
  })  : _apiKey = apiKey,
        _baseUrl = baseUrl,
        _inner = client,
        _logging = logging;

  final _log = Logger('TextSpeechAPI');

  final String _baseUrl;

  final String _apiKey;

  final http.Client _inner;

  final bool _logging;

  Future<Voice?> getVoice({required String voiceId}) async {
    final uri = Uri.parse('$_baseUrl/v1/voices/$voiceId');

    final response = await _inner.get(uri, headers: {
      'accept': 'application/json',
      'xi-api-key': _apiKey,
    });

    if (_logging) {
      _log.info('Response code: ${response.statusCode}');
      _log.info('Response body: ${response.body}');
    }

    if (response.statusCode == HttpStatus.ok) {
      return Voice.fromJson(jsonDecode(response.body));
    }

    return null;
  }

  Future<List<Voice>?> getVoices() async {
    final uri = Uri.parse('$_baseUrl/v1/voices');

    final response = await _inner.get(uri, headers: {
      'accept': 'application/json',
      'xi-api-key': _apiKey,
    });

    if (_logging) {
      _log.info('Response code: ${response.statusCode}');
      _log.info('Response body: ${response.body}');
    }

    if (response.statusCode == HttpStatus.ok) {
      final voices = Voices.fromJson(jsonDecode(response.body));
      return voices.voices;
    }

    return null;
  }

  Future<bool> deleteVoice({required String voiceId}) async {
    final uri = Uri.parse('$_baseUrl/v1/voices/$voiceId');

    final response = await _inner.delete(uri, headers: {
      'accept': 'application/json',
      'xi-api-key': _apiKey,
    });

    if (_logging) {
      _log.info('Response code: ${response.statusCode}');
      _log.info('Response body: ${response.body}');
    }

    if (response.statusCode == HttpStatus.ok) {
      return true;
    }

    return false;
  }

  Future<Voice?> addVoice({
    required String name,
    required List<List<int>> files,
  }) async {
    final uri = Uri.parse('$_baseUrl/v1/voices/add');

    var request = http.MultipartRequest('POST', uri)
      ..fields['name'] = name
      ..headers.addAll({
        'accept': 'application/json',
        'xi-api-key': _apiKey,
      });

    for (final file in files) {
      request.files.add(
        http.MultipartFile.fromBytes(
          'UploadFile',
          file,
          contentType: MediaType('audio', 'mpeg'),
        ),
      );
    }

    var response = await http.Response.fromStream(await request.send());

    if (_logging) {
      _log.info('Response code: ${response.statusCode}');
      _log.info('Response body: ${response.body}');
    }

    if (response.statusCode == 200) {
      return Voice.fromJson(jsonDecode(response.body));
    }

    return null;
  }

  Future<bool> editVoice({
    required String voiceId,
    required String name,
    required List<List<int>> files,
  }) async {
    final uri = Uri.parse('$_baseUrl/v1/voices/$voiceId/edit');

    var request = http.MultipartRequest('POST', uri)
      ..fields['name'] = name
      ..headers.addAll({
        'accept': 'application/json',
        'xi-api-key': _apiKey,
      });

    for (final file in files) {
      request.files.add(
        http.MultipartFile.fromBytes(
          'UploadFile',
          file,
          contentType: MediaType('audio', 'mpeg'),
        ),
      );
    }

    var response = await http.Response.fromStream(await request.send());

    if (_logging) {
      _log.info('Response code: ${response.statusCode}');
      _log.info('Response body: ${response.body}');
    }

    if (response.statusCode == 200) {
      return true;
    }

    return false;
  }
}

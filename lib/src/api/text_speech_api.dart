import 'dart:convert';
import 'dart:io';

import 'package:elevenlabs_sdk/src/model/text_speech_request.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

class TextSpeechAPI {
  TextSpeechAPI({
    required String baseUrl,
    required String apiKey,
    required http.Client client,
    bool logging = false,
  })  : _apiKey = apiKey,
        _baseUrl = baseUrl,
        _inner = client,
        _logging = logging;

  final _log = Logger('TextSpeechAPI');

  final String _baseUrl;

  final String _apiKey;

  final http.Client _inner;

  final bool _logging;

  Future<List<int>?> textToSpeech({
    required String voiceId,
    required String text,
    String modelId = 'eleven_multilingual_v2',
    double stability = 0.5,
    double similarityBoost = 0.5,
    double style = 0.5,
    bool useSpeakerBoost = true,
  }) async {
    final uri = Uri.parse('$_baseUrl/v1/text-to-speech/$voiceId');

    final request = TextSpeechRequest(
        text: text,
        modelId: modelId,
        voiceSettings: VoiceSettings(
          stability: stability,
          similarityBoost: similarityBoost,
          style: style,
          useSpeakerBoost: useSpeakerBoost,
        ));

    final response = await _inner.post(
      uri,
      headers: {
        'accept': 'application/json',
        'xi-api-key': _apiKey,
        'Content-Type': 'application/json'
      },
      body: jsonEncode(request.toJson()),
    );

    if (response.statusCode == HttpStatus.ok) {
      if (_logging) {
        _log.info('Response code: ${response.statusCode}');
        _log.info('Response body: ${response.body.length} bytes');
      }

      return response.bodyBytes.toList();
    } else {
      if (_logging) {
        _log.info('Response code: ${response.statusCode}');
        _log.info('Response body: ${response.body}');
      }
    }

    return null;
  }
}

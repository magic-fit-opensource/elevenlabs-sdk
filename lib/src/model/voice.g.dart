// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'voice.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Voice _$VoiceFromJson(Map<String, dynamic> json) => Voice(
      voiceId: json['voice_id'] as String,
      name: json['name'] as String?,
      description: json['description'] as String?,
      category: json['category'] as String?,
      previewUrl: json['preview_url'] as String?,
      samples: (json['samples'] as List<dynamic>?)
          ?.map((e) => Sample.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$VoiceToJson(Voice instance) => <String, dynamic>{
      'voice_id': instance.voiceId,
      'name': instance.name,
      'description': instance.description,
      'category': instance.category,
      'preview_url': instance.previewUrl,
      'samples': instance.samples,
    };

Voices _$VoicesFromJson(Map<String, dynamic> json) => Voices(
      voices: (json['voices'] as List<dynamic>)
          .map((e) => Voice.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$VoicesToJson(Voices instance) => <String, dynamic>{
      'voices': instance.voices,
    };

Sample _$SampleFromJson(Map<String, dynamic> json) => Sample(
      fileName: json['file_name'] as String,
      hash: json['hash'] as String,
      mimeType: json['mime_type'] as String,
      sampleId: json['sample_id'] as String,
      sizeBytes: json['size_bytes'] as int,
    );

Map<String, dynamic> _$SampleToJson(Sample instance) => <String, dynamic>{
      'file_name': instance.fileName,
      'hash': instance.hash,
      'mime_type': instance.mimeType,
      'sample_id': instance.sampleId,
      'size_bytes': instance.sizeBytes,
    };

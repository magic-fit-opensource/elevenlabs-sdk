import 'package:json_annotation/json_annotation.dart';

part 'text_speech_request.g.dart';

@JsonSerializable()
class TextSpeechRequest {
  const TextSpeechRequest({
    required this.text,
    required this.modelId,
    required this.voiceSettings,
  });

  final String text;

  @JsonKey(name: 'model_id')
  final String modelId;

  @JsonKey(name: 'voice_settings')
  final VoiceSettings voiceSettings;

  Map<String, dynamic> toJson() => _$TextSpeechRequestToJson(this);

  factory TextSpeechRequest.fromJson(Map<String, dynamic> json) =>
      _$TextSpeechRequestFromJson(json);
}

@JsonSerializable()
class VoiceSettings {
  const VoiceSettings({
    required this.stability,
    required this.similarityBoost,
    this.style,
    this.useSpeakerBoost,
  });

  final double stability;

  @JsonKey(name: 'similarity_boost')
  final double similarityBoost;

  final double? style;

  @JsonKey(name: 'use_speaker_boost')
  final bool? useSpeakerBoost;

  Map<String, dynamic> toJson() => _$VoiceSettingsToJson(this);

  factory VoiceSettings.fromJson(Map<String, dynamic> json) =>
      _$VoiceSettingsFromJson(json);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'text_speech_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TextSpeechRequest _$TextSpeechRequestFromJson(Map<String, dynamic> json) =>
    TextSpeechRequest(
      text: json['text'] as String,
      modelId: json['model_id'] as String,
      voiceSettings: VoiceSettings.fromJson(
          json['voice_settings'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TextSpeechRequestToJson(TextSpeechRequest instance) =>
    <String, dynamic>{
      'text': instance.text,
      'model_id': instance.modelId,
      'voice_settings': instance.voiceSettings,
    };

VoiceSettings _$VoiceSettingsFromJson(Map<String, dynamic> json) =>
    VoiceSettings(
      stability: (json['stability'] as num).toDouble(),
      similarityBoost: (json['similarity_boost'] as num).toDouble(),
      style: (json['style'] as num?)?.toDouble(),
      useSpeakerBoost: json['use_speaker_boost'] as bool?,
    );

Map<String, dynamic> _$VoiceSettingsToJson(VoiceSettings instance) =>
    <String, dynamic>{
      'stability': instance.stability,
      'similarity_boost': instance.similarityBoost,
      'style': instance.style,
      'use_speaker_boost': instance.useSpeakerBoost,
    };

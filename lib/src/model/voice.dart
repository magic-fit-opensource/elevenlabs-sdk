import 'package:json_annotation/json_annotation.dart';

part 'voice.g.dart';

@JsonSerializable()
class Voice {
  const Voice({
    required this.voiceId,
    this.name,
    this.description,
    this.category,
    this.previewUrl,
    this.samples,
  });

  @JsonKey(name: 'voice_id')
  final String voiceId;

  final String? name;

  final String? description;

  final String? category;

  @JsonKey(name: 'preview_url')
  final String? previewUrl;

  @JsonKey(name: 'samples')
  final List<Sample>? samples;

  Map<String, dynamic> toJson() => _$VoiceToJson(this);

  factory Voice.fromJson(Map<String, dynamic> json) => _$VoiceFromJson(json);

  @override
  String toString() {
    return 'Voice{voiceId: $voiceId, name: $name, description: $description, '
        'category: $category, previewUrl: $previewUrl, samples: $samples}';
  }
}

@JsonSerializable()
class Voices {
  const Voices({required this.voices});

  final List<Voice> voices;

  Map<String, dynamic> toJson() => _$VoicesToJson(this);

  factory Voices.fromJson(Map<String, dynamic> json) => _$VoicesFromJson(json);

  @override
  String toString() {
    return 'Voices{voices: $voices}';
  }
}

@JsonSerializable()
class Sample {
  const Sample({
    required this.fileName,
    required this.hash,
    required this.mimeType,
    required this.sampleId,
    required this.sizeBytes,
  });

  @JsonKey(name: 'file_name')
  final String fileName;

  final String hash;

  @JsonKey(name: 'mime_type')
  final String mimeType;

  @JsonKey(name: 'sample_id')
  final String sampleId;

  @JsonKey(name: 'size_bytes')
  final int sizeBytes;

  Map<String, dynamic> toJson() => _$SampleToJson(this);

  factory Sample.fromJson(Map<String, dynamic> json) => _$SampleFromJson(json);

  @override
  String toString() {
    return 'Sample{fileName: $fileName, hash: $hash, mimeType: $mimeType, '
        'sampleId: $sampleId, sizeBytes: $sizeBytes}';
  }
}

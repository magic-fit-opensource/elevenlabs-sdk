import 'dart:io';

import 'package:dotenv/dotenv.dart';
import 'package:elevenlabs_sdk/elevenlabs_sdk.dart';

const outputFolder = 'output';

Future<void> main() async {
  final instructors = [
    'Matt Morton',
    'Celia Quansah',
    'Asha Philips',
    'Perry Mussington',
    'Sir Alastair Cook',
    'Desiree Henry',
  ];

  final text = ['Chirag', 'David', 'Nick', 'Sunil', 'Varun'];

  final ElevenLabsSDK sdk = ElevenLabsSDK(
    apiKey: (DotEnv(includePlatformEnvironment: true)
      ..load())['ELEVEN_LABS_API_KEY']!,
  );

  var voices = await sdk.voices.getVoices();

  if (voices != null) {
    voices = voices.where((e) => instructors.contains(e.name)).toList();
  } else {
    print('Error getting voices');
    exit(-1);
  }

  final directory = Directory(outputFolder);

  if (!directory.existsSync()) {
    await directory.create();
  }

  print('Found ${voices.length} voices');

  for (final voice in voices) {
    for (final item in text) {
      final name = '${voice.name} - $item';

      final audioBytes = await sdk.textSpeech.textToSpeech(
        voiceId: voice.voiceId,
        text: item,
        similarityBoost: 1.0,
        stability: 1.0,
        style: 0.5,
        useSpeakerBoost: true,
      );

      if (audioBytes != null) {
        final fileName = '$outputFolder/$name.mp3';
        print('Writing $fileName');
        await File(fileName).writeAsBytes(audioBytes);
      } else {
        print('Error generating audio for $name');
      }
    }
  }

  print('Finished');
}

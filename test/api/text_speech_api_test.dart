import 'package:dotenv/dotenv.dart';
import 'package:elevenlabs_sdk/elevenlabs_sdk.dart';
import 'package:logging/logging.dart';
import 'package:test/test.dart';

void main() {
  setUp(() {
    Logger.root.onRecord.listen((record) {
      print('${record.level.name}: ${record.time}: ${record.message}');
    });
  });

  group('Text Speech API Test', () {
    final ElevenLabsSDK sdk = ElevenLabsSDK(
      apiKey: (DotEnv(includePlatformEnvironment: true)
        ..load())['ELEVEN_LABS_API_KEY']!,
      logging: true,
    );

    test('Text to speech', () async {
      final audio = await sdk.textSpeech.textToSpeech(
        voiceId: 'rZl76gIAIHKdpMLu77l4',
        text: 'Engineering',
      );

      expect(audio?.isNotEmpty, true);
    });
  });
}

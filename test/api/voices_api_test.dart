import 'dart:io';

import 'package:dotenv/dotenv.dart';
import 'package:elevenlabs_sdk/elevenlabs_sdk.dart';
import 'package:test/test.dart';

void main() {
  group('Voices API Test', () {
    final ElevenLabsSDK sdk = ElevenLabsSDK(
        apiKey: (DotEnv(includePlatformEnvironment: true)
          ..load())['ELEVEN_LABS_API_KEY']!);

    test('Get voices', () async {
      final voices = await sdk.voices.getVoices();
      print(voices);
      expect(voices?.isNotEmpty, true);
    });

    test('Get voice', () async {
      final voice = await sdk.voices.getVoice(voiceId: '9tic8rYl8Jy0pItGSaE6');
      print(voice);
      expect(voice?.voiceId, '9tic8rYl8Jy0pItGSaE6');
    });

    // test('Delete voice', () async {
    //   final result = await sdk.voices.deleteVoice(voiceId: '');
    //   expect(result, true);
    // });

    test('Add voice / Delete voice', () async {
      Directory directory = Directory('test/test_data/matt_morton');
      final files = directory.listSync();

      List<List<int>> filesBytes = [];

      for (final file in files) {
        final bytes = File(file.path).readAsBytesSync();
        filesBytes.add(bytes.toList());
      }

      final voice = await sdk.voices
          .addVoice(name: 'Matt Morton Test Add Delete', files: filesBytes);

      expect(voice?.voiceId.isNotEmpty, true);

      print(voice);

      final result = await sdk.voices.deleteVoice(voiceId: voice!.voiceId);
      expect(result, true);
    });

    test('Edit voice', () async {
      Directory directory = Directory('test/test_data/matt_morton');
      final files = directory.listSync();

      List<List<int>> filesBytes = [];

      for (final file in files) {
        final bytes = File(file.path).readAsBytesSync();
        filesBytes.add(bytes.toList());
      }

      final result = await sdk.voices.editVoice(
          voiceId: '9tic8rYl8Jy0pItGSaE6',
          name: 'Matt Morton Test Add Delete',
          files: filesBytes);

      print(result);

      expect(result, true);
    });
  });
}
